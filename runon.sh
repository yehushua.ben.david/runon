#!/bin/bash

function print_help() {
	echo -e "Usage:  $0 -h host [option...] commands
Options:
	-h --host   :  host
	-i --id     :  private key
	-v --volume :  local_volune:host_volume
	-e --env    :  VAR=VALUE 
Example:
$0 -h root@server1 -h root@server2 -v ./data:/tmp/data -v ./code:~/code 'cd ~/code; bash -x runthis.sh | cat -n'
"
	exit
}

if [ $# -eq 0 ]; then
	print_help
fi
